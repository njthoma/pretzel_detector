## Summary

The pretzel detector takes in images of pretzels in an office environment, and
attempts to form a bounding box around each pretzel in the image. The detector
uses OpenCV to extract the 'pretzel' colored pixels from each image, and fits
bounding boxes around the pixel clusters. The 'pretzel' color was 
determined heuristically through examination of the histograms representing the 
HSV components of each example image. A generalized range of HSV values was 
selected and can be used to isolate the 'pretzel' color in a given image. 
Results of the pretzel detector on each example image can be found in the 
`results/` folder.

## Instructions

    `python3 main.py -i images/image0.jpg`

The image file can be specified using the `-i` flag (default: 
'images/image0.jpg'). All image files are located in the `images/` folder.

## Dependencies

* Python 3.6
* NumPy, Matplotlib
