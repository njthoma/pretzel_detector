#!/usr/bin/env python

import argparse
import sys
import signal
import cv2
import numpy as np
import heapq
import pylab
import matplotlib
from matplotlib import pyplot as plt
from matplotlib import colors

class SigHandler(object):
    def __init__(self):
        """Inits SigHandler with cntl+c exit handler."""
        signal.signal(signal.SIGINT, self.kill_handler)

    def kill_handler(self, signal, frame):
        """Safely exits program."""
        sys.exit(0)

def main(image):
    """Receives an image containing pretzels and attempts to overlay a bounding 
       box around each pretzel in the image.

    Args:
        image: String containing the image file location 
               (default: 'images/image0.jpg')
    """
    exit_handler = SigHandler()
    img = cv2.imread(image)

    # filter noise and convert to hsv
    img_blur = cv2.GaussianBlur(img, (15, 15), 0)
    hsv = cv2.cvtColor(img_blur, cv2.COLOR_BGR2HSV)

    # isolate brown pixels (heuristic)
    min_brown = np.array([6,100,88])
    max_brown = np.array([14,255,255])
    mask = cv2.inRange(hsv, min_brown, max_brown)
    out = cv2.bitwise_and(hsv, hsv, mask=mask)

    # find contours and bounding boxes
    _, contours, hierarchy = cv2.findContours(mask, cv2.RETR_LIST, 
                                              cv2.CHAIN_APPROX_SIMPLE)
    bounding_boxes = [cv2.boundingRect(c) for c in contours]

    # compute average over n=50 largest box areas
    area = []
    n = 50
    for idx,bb in enumerate(bounding_boxes):
        w,h = bb[2], bb[3]
        heapq.heappush(area, w*h)
    avg_area = sum(heapq.nlargest(n, area)) / n

    # display boxes with area greater than n=50 average
    for bb in bounding_boxes:
        x,y,w,h = bb[0], bb[1], bb[2], bb[3]
        if w*h > avg_area:
            cv2.rectangle(img,(x,y),(x+w,y+h), (255,0,0), 15)

    # plot resulting image
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    plt.figure(figsize=(5,5))
    plt.imshow(img)
    pylab.show()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--image', type=str,
                        default='images/image0.jpg',help='input image file')
    args = parser.parse_args()
    main(**vars(args))
